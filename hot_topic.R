library(NLP)
library(tm)
library(openNLP)
library(graph)

tagPOS <-  function(x, ...) {
  s <- as.String(x)
  word_token_annotator <- Maxent_Word_Token_Annotator()
  a2 <- Annotation(1L, "sentence", 1L, nchar(s))
  a2 <- annotate(s, word_token_annotator, a2)
  a3 <- annotate(s, Maxent_POS_Tag_Annotator(), a2)
  a3w <- a3[a3$type == "word"]
  POStags <- unlist(lapply(a3w$features, `[[`, "POS"))
  POStagged <- paste(sprintf("%s/%s", s[a3w], POStags), collapse = " ")
  list(POStagged = POStagged, POStags = POStags)
}

SplitText <- function(Phrase) { 
  unlist(strsplit(Phrase," "))
}

trim <- function (x) gsub("^\\s+|\\s+$", "", x)

IsPunctuated <- function(Phrase) {
  length(grep("\\.|,|!|\\?|;|:|\\)|]|}\\Z",Phrase,perl=TRUE))>0 # punctuation: . , ! ? ; : ) ] }
}

SelectTaggedWords <- function(Words,tagID) {
  Words[ grep(tagID,Words) ]
}

RemoveTags <- function(Words) {
  sub("/[A-Z]{2,3}","",Words)
}

IsSelectedWord <- function(Word) {
  ifelse(length(which(selected_words == Word))>0, TRUE, FALSE)
}

GetWordLinks <- function(position,scope) {
  scope <- ifelse(position+scope>length(words),length(words),position+scope)
  print('position')
  print(position)
  print('hehe')
  print('scope')
  print(scope)
  print('hehoaaa')
  links <- ""
  for (i in (position+1):scope) {
    if ( IsSelectedWord(words[i]) ) 
      links <- c(links,words[i])
    
  }
  
  if (length(links)>1) {
    links[2:length(links)]
  }
  else {
    links <- ""
  }
  # print(links)  
}

ConstructTextGraph <- function(n) { 
  word_graph <- new("graphNEL")
  i <- 1
  while (i < length(words) ) {
    if ( IsSelectedWord(words[i]) ) {                                   
      links <- GetWordLinks(i,n)
      print('links')
      print(links)
      if (links[1] != "") { 
        print('$$$$$$$$$$$')
        cat(i," ",words[i]," - ",paste(c(links),collapse=" "),"\n")
        if ( length(which(nodes(word_graph)==words[i]))==0  ) {     
          word_graph <- addNode(words[i],word_graph)
          print(word_graph)
        }                                               
        
        for (j in 1:length(links)) {
          if ( length(which(nodes(word_graph)==links[j]))==0 ) {
            word_graph <- addNode(links[j],word_graph)
            word_graph <- addEdge(words[i],links[j],word_graph,1)
            print(word_graph)
          } 
          else {
            if ( length(which(edges(word_graph,links[j])[[1]]==words[i]))>0 ) { 
              prev_edge_weight <- as.numeric(edgeData(word_graph,words[i],links[j],"weight"))
              edgeData(word_graph,words[i],links[j],"weight") <- prev_edge_weight+1
              print('prev_edge_weight')
              print(prev_edge_weight)
              print('wow')
            }
            else {
              word_graph <- addEdge(words[i],links[j],word_graph,1)
              print(word_graph)
            }
          } 
        }
      }
    }
    i <- i+1
  }
  word_graph
}

#### Reading text documents ######
setwd("/home/navjot.kaur/Desktop")
publications <- read.csv("demo_data.csv", sep='\t')
doc <- publications$Comment
#doc <- as.character(doc)
#library(qdap)
#sent <- sent_detect(doc)
#doc <- sent

################################
corp <- Corpus(VectorSource(doc))
corp <- tm_map(corp, stripWhitespace)
corp <- tm_map(corp, tolower)
words_with_punctuation <- SplitText(as.character(corp))
corp <- tm_map(corp, removePunctuation)


#--- GRAPH CONSTRUCTION
#words <- SplitText(as.character(corp))
tagged_text <- tagPOS(corp)
tagged_words <- SplitText(as.character(tagged_text))
tagged_words <- c(SelectTaggedWords(tagged_words,"/NN"),SelectTaggedWords(tagged_words,"/JJ"))  # keep only NN & JJ tagged words 
tagged_words <- RemoveTags(tagged_words)                                                        # remove un-used tag POS
selected_words <- unique(tagged_words)                                                          
text_graph <- ConstructTextGraph(2)




## Visualize obtained text graph
library("Rgraphviz")
#source("http://bioconductor.org/biocLite.R")
#biocLite("Rgraphviz")
plot(text_graph, attrs = list(node = list(fillcolor = "lightblue", fontsize = 20),edge = list(arrowsize=0.5)))



# ---  PAGE RANK
d <- 0.90                               # damping factor
threshold <- 1e-4               # convergence threshold 
text_nodes <- nodes(text_graph)
print('text nodes')
print(text_nodes)
nodes_num <- length(text_nodes)
print('node num is ')
nodes_num
nodes_rank <- matrix(1,nodes_num,2)
print('node rank')
nodes_rank

k <- 0                                  # iterations
convergence_reached <- FALSE
repeat {
  for (i in 1:nodes_num) {
    incoming_link <- adj(text_graph,text_nodes[i])[[1]]
    print('incoming link')
    print(incoming_link)
    incoming_num <- length(incoming_link)
    print('incoming_num is')
    print(incoming_num)
    tmp <- 0
    for (j in 1:incoming_num) {
      link_num <- which(text_nodes==incoming_link[j])
      print('link num')
      link_num
      outgoing_num <- length(adj(text_graph,text_nodes[link_num])[[1]])
      print('outgoing_num')
      outgoing_num
      tmp <- tmp + nodes_rank[link_num,1] / outgoing_num
      print('temp score')
      tmp
    }
    nodes_rank[i,1] <- (1-d)+d*tmp
  }
  k <- k+1
  for (i in 1:nodes_num) {
    if (abs(nodes_rank[i,1]-nodes_rank[i,2])<threshold) 
      convergence_reached <- TRUE
  }
  if (convergence_reached) break
  nodes_rank[,2] <- nodes_rank[,1]
}
# --- POST-PROCESSING
keywords_num <- round(nodes_num/3) # a third of the number of vertices in the graph.
ranked_words <- data.frame(text_nodes,nodes_rank[,1])
names(ranked_words) <- c("word","rank")
strong_words <- ranked_words[order(ranked_words$rank,decreasing=TRUE),]
print(strong_words)
strong_words <- as.character(strong_words$word[1:keywords_num])
keywords <- ""
keywords_scores <- 0
for (i in 1:keywords_num) {
  keyword_positions <- which(words==strong_words[i])
  for (j in 1:length(keyword_positions)) {
    keyword <- ""
    keyword_score <- 0
    k <- keyword_positions[j]                                       
    repeat {
      if (IsSelectedWord(words[k])) { 
        keyword <- trim(paste(c(keyword,words[k]),collapse=" "))
        keyword_score <- keyword_score + ranked_words[which(ranked_words$word==words[k]),2]
      }
      else break                                                    
      
      if (IsPunctuated(words_with_punctuation[k])) break
      if (k==length(words)) break                               
      k <- k+1
    }
    k <- keyword_positions[j]-1                                 
    repeat {
      if (k<1) break
      
      if (IsSelectedWord(words[k])) { 
        keyword <- paste(c(words[k],trim(keyword)),collapse=" ")
        keyword_score <- keyword_score + ranked_words[which(ranked_words$word==words[k]),2]
      }
      else break
      
      if (k>1) {            
        if (IsPunctuated(words_with_punctuation[k-1])) break
      } 
      k <- k-1
    }
    if (keyword!=strong_words[i]) { 
      keywords <- c(keywords,keyword)
      keywords_scores <- c(keywords_scores,keyword_score)
    }   
  }
}
keywords_df <- data.frame(keywords,keywords_scores)
keywords_list <- keywords_df[order(keywords_df$keywords_scores,decreasing=TRUE),] 
keywords_list <- unique(as.character(keywords_list$keywords[1:nrow(keywords_list)]))  
sort(keywords_list)
tokens <- sort(keywords_list)
index <- as.logical(ifelse(frequency(tokens) < 4, 1, 0))
tokens <- tokens[index]

