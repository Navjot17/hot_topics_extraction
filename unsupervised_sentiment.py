from __future__ import division
import re
import csv
import glob
import numpy as np
import nltk
from cleanup_utils import get_cleanup_string
list3=['no''not','dont','willnot','didnot','does not','did not','doesnt','hasnt','hadnt','havent','wasnt']
def read_pos_neg_words(filename):
  file_buffer = open(filename)
lines = []
for line in file_buffer:
  if line[0] in ['#', ' ', ';']:
  continue
line = line.strip()
if line:
  lines.append(line)
file_buffer.close()
return lines


def read_documents(input_file):
  file_data = []
for line in csv.reader(open(input_file), delimiter='\t'):
  file_data.append(line)
return file_data


def write_output(outfile, data):
  csv_writer = csv.writer(open(outfile, 'wb'), delimiter='\t')
csv_writer.writerows(data)


class CreatePosNegMatrix(object):
  def __init__(self, hot_topics,pos_file, neg_file,amp_file,deamp_file,input_files, sparsity_val=95):
  self.hot_topics =read_pos_neg_words(hot_topics)
#        print self.hot_topics
self.positive_words = read_pos_neg_words(pos_file)
self.negative_words = read_pos_neg_words(neg_file)
self.amplificationwords = read_pos_neg_words(amp_file)
self.deamplificationwords = read_pos_neg_words(deamp_file)
self.input_files = input_files
self.sparsity_val = sparsity_val
self.pos_matrix = [self.positive_words]
self.neg_matrix = [self.negative_words]
self.collated_results = []

def create_matrix(self):
  
  for doc in self.input_files:
  print doc
print '___________________________________'
posScore = 1 
negScore = 1
doc = get_cleanup_string(doc)
doc_word_count = doc.count(' ')
pos = [1 if re.search(r'\b%s\b' % re.escape(term), doc, re.I) else 0
       for term in self.positive_words]    
neg = [1 if re.search(r'\b%s\b' % re.escape(term), doc, re.I) else 0
       for term in self.negative_words]
hot_topics = [1 if re.search(r'\b%s\b' % re.escape(term), doc, re.I) else 0
              for term in self.hot_topics]
hot_topics=[]                  
for terms in self.hot_topics:  
  if terms in doc:
  print terms  
hot_topics.append(terms) 
else:
  continue    
tokens = doc.split()
posScore_temp = 0
negScore_temp = 0
negationPresent = 0        
for i in range(1,len(tokens)):
  current = tokens[i]
previous = tokens[i-1]       
if (current in self.positive_words):
  print "positive word"+'->'+current
print "*********"
posScore_temp +=1
if(previous in self.deamplificationwords):
  print "demplificationwords"+'->'+current
print "*********"
posScore_temp -=0.2 
elif(previous in self.amplificationwords):
  print "amplificationwords"+'->'+current
print "*********"
posScore_temp +=0.2
elif (current in self.negative_words):
  print "negative word"+'->'+current
print '$$$$$$4'   
negScore_temp +=1
if (previous in self.amplificationwords):
  print "amplificationwords"+'->'+current
print "*********"
negScore_temp +=0.2    
elif (previous in self.deamplificationwords):
  print "demplificationwords"+'->'+current
print "*********"
negScore_temp -=0.2                      
elif (current in list3):
  print "negationword"+'->'+current
print '^^^^^^' 
negationPresent += 1

if posScore_temp>0:
  if negationPresent>posScore_temp:
  negationPresent = posScore_temp
posScore_temp -= negationPresent
posScore += posScore_temp
negScore += negationPresent
if negScore_temp>0:
  if negationPresent>negScore_temp:
  negationPresent = negScore_temp
negScore_temp -= negationPresent
negScore += negScore_temp
posScore += negationPresent
result = float(posScore)/float(negScore)
print 'sentimentscore is :'
print result
score=[]
score.append(result) 
sentiment =[]
pos_count = sum(pos)
neg_count = sum(neg)
if ((pos_count + neg_count) !=0): 
  if(result >= 1):
  print 'pos'  
sentiment.append('pos')     
else:    
  print 'neg' 
sentiment.append('neg')
else:
  print 'Null' 
sentiment.append('Null')     
self.pos_matrix.append(pos)
self.neg_matrix.append(neg)
(self.collated_results.append([doc_word_count, pos_count, neg_count,
                               doc_word_count -
                                 pos_count - neg_count,score,sentiment,doc,hot_topics]))

def get_sparsity_matrix(self):
  pos_array = self.__sparsity_matrix(
    np.array(self.pos_matrix).transpose())
neg_array = self.__sparsity_matrix(
  np.array(self.neg_matrix).transpose())
pos_array = np.array(pos_array).transpose()
neg_array = np.array(neg_array).transpose()
write_output('pos_feature_matrix_erbitux.csv', pos_array)
write_output('neg_feature_matrix_erbitux.csv', neg_array)



def __sparsity_matrix(self, nparray):
  sparse_matrix = []
for col in nparray:
  val_counts = nltk.FreqDist(col[1:])
if val_counts[0] * 100 / len(col[1:]) < self.sparsity_val:
  sparse_matrix.append(col)
return sparse_matrix


if __name__ == '__main__':
  pos_filename = '/home/innoplexus/Downloads/Downloads/clarity/Ajay/dataforopinionmining/Hu_Liu_positive_word_list.txt'
neg_filename = '/home/innoplexus/Downloads/Downloads/clarity/Ajay/dataforopinionmining/Hu_Liu_negative_word_list.txt'
amp_filename='/home/innoplexus/Downloads/Downloads/clarity/Ajay/Vectabix data analysis/vect_results _1000_topics/fwddictionary/amplificationwords.csv'
deamp_filename='/home/innoplexus/Downloads/Downloads/clarity/Ajay/Vectabix data analysis/vect_results _1000_topics/fwddictionary/deamplificationwords.csv'
hot_topics_filename='/home/innoplexus/Downloads/Downloads/clarity/Ajay/Vectabix data analysis/Filt_topics_70.csv'
pos_file_path = '/home/innoplexus/Downloads/Downloads/clarity/Ajay/Vectabix data analysis/vect_results _1000_topics/erbituxvectabixsa/erbitux_comments/'
#    neg_file_path = '/home/innoplexus/Downloads/Downloads/clarity/Ajay/reviews/reviews/train/unsup/'
#    print neg_file_path
out_filename = 'document_frequency_count_erbitux_finals.csv'
# input_filename = 'dailystrength_clarity_comment_data.csv'
inputfiles = glob.glob(pos_file_path + '*.txt')
#       glob.glob('neg_file_path' + '*.txt')
input_data = [open(i).read() for i in inputfiles]
posneg_object = CreatePosNegMatrix(hot_topics_filename,pos_filename, neg_filename,amp_filename,deamp_filename,input_data)
posneg_object.create_matrix()
collated_results = posneg_object.collated_results
csv_data = [['Documents', 'Total_words_cout', 'Positive_count',
             'Negative_count', 'Other_words_count','score','sentiment','comments']]
for num, doc_file in enumerate(inputfiles):
  doc_name = doc_file.split('/')[-1]
csv_data.append([doc_name] + collated_results[num])
write_output(out_filename, csv_data)
posneg_object.get_sparsity_matrix()
